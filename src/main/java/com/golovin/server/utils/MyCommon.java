package com.golovin.server.utils;

import com.golovin.server.Services.*;


public class MyCommon {

    private MyCommon() {

    }
    public static ChaptersService getChapterService(){
        return chapterService;
    }
    public static UserService getUserService(){
        return userService;
    }
    public static BookService getBookService(){
        return bookService;
    }
    public static CompletenessService getCompletenessService(){
        return completenessService;
    }
    public static GenresService getGenresService(){
        return genresService;
    }
    public static LastBookService getLastBookService(){
        return lastBookService;
    }
    public static LibraryService getLibraryService(){
        return libraryService;
    }
    public static StatusService getStatusService(){
        return statusService;
    }

    public static CoverService getCoverService() {return coverService;}

    private static ChaptersService chapterService = new ChaptersService();
    private static UserService userService = new UserService();
    private static BookService bookService = new BookService();
    private static CompletenessService completenessService = new CompletenessService();

    public static CoverService coverService = new CoverService();
    public static GenresService genresService = new GenresService();
    private static LastBookService lastBookService = new LastBookService();
    private static LibraryService libraryService = new LibraryService();
    private static StatusService statusService = new StatusService();


}
