package com.golovin.server.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "статус")
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "статус_книги")
    private String status;

    public Status() {

    }

}
