package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface GenresDaoInterface<T, Id extends Serializable> {
    public T findById(Session session, Id id);

    public List<T> findAll(Session session);

}
