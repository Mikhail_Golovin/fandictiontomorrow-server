package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface LastBookDaoInterface <T, Id extends Serializable> {
    public void persist(Session session, T entity);

    public void update(Session session, T entity);

    public void delete(Session session, T entity);

    public List<T> findAll(Session session);

    public List<T> findUserLastBooks(Session session, Id id);

    public T findBookWithMaxPriority(Session session, Id id);

    public T findBookWithMinPriority(Session session, Id id);
}
