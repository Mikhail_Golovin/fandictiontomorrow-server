package com.golovin.server.Services;

import com.golovin.server.DAO.BookDao;
import com.golovin.server.classes.*;
import com.golovin.server.models.*;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
public class BookService {

    private static BookDao bookDao;


    public BookService() {
        bookDao = new BookDao();
    }

    public Books getById(int id) {
        Session session = bookDao.openCurrentSession();
        Books book = bookDao.findById(session, id);
        bookDao.closeCurrentSession(session);
        return book;
    }

    private void persist(Books book) {
        Session session = bookDao.openCurrentSessionWithTransaction();
        bookDao.persist(session, book);
        bookDao.closeCurrentSessionWithTransaction(session);
    }

    public void update(Books book) {
        Session session = bookDao.openCurrentSessionWithTransaction();
        bookDao.update(session, book);
        bookDao.closeCurrentSessionWithTransaction(session);
    }

    public void delete(Books book) {
        Session session = bookDao.openCurrentSessionWithTransaction();
        bookDao.delete(session,book);
        bookDao.closeCurrentSessionWithTransaction(session);
    }

    public List<Books> findAll() {
        Session session = bookDao.openCurrentSession();
        List<Books> books = bookDao.findAll(session);
        bookDao.closeCurrentSession(session);
        return books;
    }

    public boolean isGenrePresent(int genreId) {

        Session session = bookDao.openCurrentSession();
        boolean isPresent = bookDao.isGenrePresent(session, genreId);
        bookDao.closeCurrentSession(session);

        return isPresent;
    }

    public ListPresentBookInGenre getPresentGenres() {
        List<Genres> genresList = MyCommon.getGenresService().findAll();
        ListPresentBookInGenre bookAndGenres = new ListPresentBookInGenre();
        for (Genres genre : genresList) {
            bookAndGenres.getGenres().add(new PresentBookInGenre(genre.getId(), isGenrePresent(genre.getId())));
            System.out.println(genre.getId() + " " + isGenrePresent(genre.getId()));
        }
        return bookAndGenres;
    }

    public ListBooksId getBooksFromLibraryWithStatus(int statusId, int userId) {

        List<Library> library = MyCommon.getLibraryService().getBooksFromLibraryWithStatus(statusId, userId);
        ListBooksId books = new ListBooksId();
        for (Library libraryEntity : library) {
            books.getBooksId().add(libraryEntity.getBook().getId());
        }

        return books;

    }


    public boolean setDraft(int bookId) {
        Books book = getById(bookId);
        book.setDraft(true);
        update(book);
        return book.isDraft();
    }

    public BookClass getBook(int bookId, int userId) {
        Session session = bookDao.openCurrentSession();
        Books sqlBook = bookDao.findById(session, bookId);
        bookDao.closeCurrentSession(session);
        BookClass book;
        if (sqlBook != null) {
            sqlBook.setViews(sqlBook.getViews() + 1);
            book = new BookClass(sqlBook.getName(), sqlBook.getAuthor().getId(), sqlBook.getAnnotation(),
                    sqlBook.getGenre().getId(), sqlBook.getCompleteness().getId(), sqlBook.getViews(), sqlBook.getDate().toString(),
                    sqlBook.getCover().getId(), sqlBook.isDraft());
            Session session1 = bookDao.openCurrentSession();
            session1.beginTransaction();
            bookDao.update(session1, sqlBook);
            session1.getTransaction().commit();
            session1.close();


            if (userId != -1) {
                MyCommon.getLastBookService().updateLastBooks(bookId, userId);
            }
        } else {
            System.out.println("Книга не найдена");
            book = new BookClass("Книга не найдена", 0, " ", 1, 1, 0, "0",
                    0, false);
        }
        return book;
    }

    public MainPageBook getMainPageBook(int bookId) {
        Books sqlBook = getById(bookId);
        MainPageBook book;
        if (sqlBook != null) {
            book = new MainPageBook(sqlBook.getName(), sqlBook.getAuthor().getId(), sqlBook.getCover().getId(),
                    sqlBook.isDraft());
        } else {
            book = null;
        }
        return book;
    }

    public LibraryBook getLibraryBook(int userId, int bookId) {
        int statusId = MyCommon.getLibraryService().getUserBookStatus(userId, bookId);
        Session session = bookDao.openCurrentSession();
        Books libraryBook = bookDao.getUserBook(session, userId, bookId);
        LibraryBook book;
        if (libraryBook != null) {
            book = new LibraryBook(libraryBook.getName(), libraryBook.getAuthor().getId(),
                    libraryBook.getCover().getId(), statusId, libraryBook.isDraft(), libraryBook.getGenre().getId(),
                    libraryBook.getCompleteness().getId());
        } else {
            book = new LibraryBook();
            book.setStatusId(5);
        }
        bookDao.closeCurrentSession(session);
        return book;
    }

    public SearchBook getSearchBook(int bookId, int sortType) {
        Books sqlBook = getById(bookId);
        SearchBook book;
        book = new SearchBook();
        if (sqlBook != null) {
            if (sortType == 1) {
                book = new SearchBook(sqlBook.getName(), sqlBook.getAuthor().getId(),
                        sqlBook.getAnnotation(), sqlBook.getGenre().getId(),
                        sqlBook.getViews(), sqlBook.getCover().getId(), sqlBook.isDraft(), Long.MAX_VALUE - sqlBook.getViews());
            } else if (sortType == 2) {
                book = new SearchBook(sqlBook.getName(), sqlBook.getAuthor().getId(),
                        sqlBook.getAnnotation(), sqlBook.getGenre().getId(),
                        sqlBook.getViews(), sqlBook.getCover().getId(), sqlBook.isDraft(), sqlBook.getDate().getTime());

            }
        } else {
            book.setAuthorId(0);
            book.setIsDraft(true);
        }
        return book;
    }

    public ListBooksId getAuthorBooks(int userId) {
        Session session = bookDao.openCurrentSession();
        List<Books> books = bookDao.getAuthor(session, userId);
        bookDao.closeCurrentSession(session);
        ListBooksId booksId = new ListBooksId();
        for (Books book : books) {
            booksId.getBooksId().add(book.getId());
        }

        return booksId;
    }

    public ListBooksId getGenreBooks(int genreId) {
        Session session = bookDao.openCurrentSession();
        List<Books> books = bookDao.getGenre(session, genreId);
        session.close();
        ListBooksId booksId = new ListBooksId();
        for (Books book : books) {
            booksId.getBooksId().add(book.getId());
        }

        return booksId;
    }

    public ListChapterInBook getChaptersOfBook(int bookId) {
        Session session = bookDao.openCurrentSession();
        Books book = bookDao.findById(session, bookId);
        bookDao.closeCurrentSession(session);
        ListChapterInBook listChapterInBook = new ListChapterInBook();
        List<Chapters> chapters = book.getChaptersList();
        for (Chapters chapter : chapters) {
            listChapterInBook.getChapters().add(new ChaptersInBook(chapter.getId(),
                    chapter.getName(), chapter.getNumber()));
        }

        return listChapterInBook;
    }

    public ListBooksId getBookInSection(Integer sectionId) {
        ListBooksId listBooksId = new ListBooksId();
        Session session = bookDao.openCurrentSession();
        List<Books> books = bookDao.getBookInSection(session, sectionId);
        bookDao.closeCurrentSession(session);
        for (Books book : books) {
            listBooksId.getBooksId().add(book.getId());
        }
        return listBooksId;
    }

    public int addBook(BookClass bookClass) {
        if (isParametersThatGoBeyondFSConditions(bookClass)) {
            return -1;
        }

        Genres genre = MyCommon.getGenresService().getById(bookClass.getGenreId());

        User author = MyCommon.getUserService().getById(bookClass.getAuthorId());
        Completeness completeness = MyCommon.getCompletenessService().getById(bookClass.getCompletenessId());
        CoverService coverService = MyCommon.getCoverService();
        Cover cover = coverService.getById(bookClass.getCoverId());
        Date date = Date.valueOf(LocalDate.now());
        Books book = new Books(author, bookClass.getName(), bookClass.getAnnotation(), genre, completeness, cover, 0, date, bookClass.getIsDraft());
        coverService.update(cover);
        persist(book);
        return book.getId();
    }

    private static boolean isParametersThatGoBeyondFSConditions(BookClass bookClass) {
        return !(bookClass.getName().length() >= 1 && bookClass.getName().length() <= 150) ||
                !(bookClass.getAnnotation().length() <= 1000 && bookClass.getAnnotation().length() >= 1);
    }

    public int changeBook(int bookId, BookClass bookJson) {
        if (isParametersThatGoBeyondFSConditions(bookJson)) {
            return -1;
        }
        Genres genre = MyCommon.getGenresService().getById(bookJson.getGenreId());
        Completeness completeness = MyCommon.getCompletenessService().getById(bookJson.getCompletenessId());
        Cover cover = MyCommon.getCoverService().getById(bookJson.getCoverId());
        Books sqlBook = getById(bookId);

        Date date = Date.valueOf(LocalDate.now());
        sqlBook.setName(bookJson.getName());
        sqlBook.setAnnotation(bookJson.getAnnotation());

        sqlBook.setGenre(genre);
        sqlBook.setCompleteness(completeness);
        sqlBook.setDate(date);
        sqlBook.setDraft(bookJson.getIsDraft());
        sqlBook.setCover(cover);
        update(sqlBook);

        return bookId;
    }

    public ListBooksId searchBooks(SearchParamsClass searchParamsClass) {
        Session session = bookDao.openCurrentSession();
        List<Books> books = bookDao.searchBooks(session, searchParamsClass.getText(), searchParamsClass.getGenreId(), searchParamsClass.getSortingType());
        bookDao.closeCurrentSession(session);
        ListBooksId listBooksId = new ListBooksId();
        for (Books book : books) {
            listBooksId.getBooksId().add(book.getId());
        }
        return listBooksId;
    }
}
