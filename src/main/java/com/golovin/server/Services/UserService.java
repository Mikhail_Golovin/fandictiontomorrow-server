package com.golovin.server.Services;

import com.golovin.server.DAO.UserDao;
import com.golovin.server.classes.UserClass;
import com.golovin.server.models.Books;
import com.golovin.server.models.Chapters;
import com.golovin.server.models.User;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class UserService {

    private static UserDao userDao = new UserDao();

    public UserService() {
    }


    public User getById(int id) {
        Session session = userDao.openCurrentSession();
        User user = userDao.findById(session, id);
        userDao.closeCurrentSession(session);
        return user;
    }


    public void persist(User user) {
        Session session = userDao.openCurrentSession();
        session.beginTransaction();
        userDao.persist(session, user);
        session.getTransaction().commit();
        session.close();
    }

    public void update(User user) {
        Session session = userDao.openCurrentSessionWithTransaction();
        userDao.update(session, user);
        userDao.closeCurrentSessionWithTransaction(session);
    }

    public List<User> findAll() {
        Session session = userDao.openCurrentSession();
        List<User> users = userDao.findAll(session);
        userDao.closeCurrentSession(session);
        return users;
    }

    public UserClass getUser(int userId) {
        User sqlUser = getById(userId);
        UserClass user;
        if (sqlUser != null) {
            user = new UserClass(sqlUser.getName(), sqlUser.getSurname());
        } else {
            user = new UserClass("Пожалуйста", "Авторизуйтесь");
        }
        return user;
    }

    public Integer findUserAuth(String name, String surname, String googleAuth) {
        User user = new User(name, surname, googleAuth);
        Session session = userDao.openCurrentSession();
        Integer userid = userDao.findUserAuth(session, user);
        userDao.closeCurrentSession(session);
        if (userid == null) {
            persist(user);
            userid = user.getId();
        }
        return userid;
    }

    public int getLastChapter(int userId, int bookId) {
        if (userId == -1){
            Chapters firstChapter = MyCommon.getChapterService().findFirstChapter(bookId);
            return firstChapter.getId();
        }
        User user = getById(userId);
        List<Chapters> chapters = user.getUserChaptersList();
        System.out.println();
        for (Chapters chapter : chapters) {
            if (chapter.getBook().getId() == bookId) {
                return chapter.getId();
            }
        }
        Chapters firstChapter = MyCommon.getChapterService().findFirstChapter(bookId);
        return firstChapter.getId();
    }

    public void changeLastChapter(int userId, int chapterId) {
        if (userId == -1){
            return;
        }
        User user = getById(userId);
        List<Chapters> chapters = user.getUserChaptersList();
        Chapters chapterToAdd = MyCommon.getChapterService().getById(chapterId);
        boolean deleteFlag = false;
        Chapters chapterToDelete = new Chapters();
        for (Chapters chapter : chapters) {
            if (chapter.getBook().getId() == chapterToAdd.getBook().getId()) {
                chapterToDelete = chapter;
                deleteFlag = true;
                break;
            }
        }
        if (deleteFlag){
            user.removeChapter(chapterToDelete);
        }
        user.addChapter(chapterToAdd);
        persist(user);
    }
}
