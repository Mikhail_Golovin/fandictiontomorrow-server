package com.golovin.server.Services;

import com.golovin.server.DAO.LastBookDao;
import com.golovin.server.classes.ListBooksId;
import com.golovin.server.models.Books;
import com.golovin.server.models.LastBook;
import com.golovin.server.models.User;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class LastBookService {
    private static LastBookDao lastBookDao = new LastBookDao();

    public LastBookService() {
    }

    private void persist(LastBook lastBook) {
        Session session = lastBookDao.openCurrentSessionWithTransaction();
        lastBookDao.persist(session, lastBook);
        lastBookDao.closeCurrentSessionWithTransaction(session);
    }

    private void update(LastBook lastBook) {
        Session session = lastBookDao.openCurrentSessionWithTransaction();
        lastBookDao.update(session, lastBook);
        lastBookDao.closeCurrentSessionWithTransaction(session);
    }

    private void delete(LastBook lastBook) {
        Session session = lastBookDao.openCurrentSessionWithTransaction();
        lastBookDao.delete(session, lastBook);
        lastBookDao.closeCurrentSessionWithTransaction(session);
    }

    public List<LastBook> findAll() {
        Session session = lastBookDao.openCurrentSession();
        List<LastBook> lastbooks = lastBookDao.findAll(session);
        lastBookDao.closeCurrentSession(session);
        return lastbooks;
    }

    public List<LastBook> getLastBook(int userId) {
        Session session = lastBookDao.openCurrentSession();
        List<LastBook> lastbooks = lastBookDao.findUserLastBooks(session, userId);
        lastBookDao.closeCurrentSession(session);
        return lastbooks;
    }

    public LastBook getBookWithMinPriority(int userId){
        Session session = lastBookDao.openCurrentSession();
        LastBook lastbook = lastBookDao.findBookWithMinPriority(session, userId);
        lastBookDao.closeCurrentSession(session);
        return lastbook;
    }

    public int getMinPriority(int userId){
        Session session = lastBookDao.openCurrentSession();
        LastBook lastbook = lastBookDao.findBookWithMinPriority(session, userId);
        lastBookDao.closeCurrentSession(session);
        if (lastbook == null)
        {
            return -1;
        }
        return lastbook.getPriority();
    }

    public int getMaxPriority(int userId){
        Session session = lastBookDao.openCurrentSession();
        LastBook lastbook = lastBookDao.findBookWithMaxPriority(session, userId);
        lastBookDao.closeCurrentSession(session);
        if (lastbook == null)
        {
            return -1;
        }
        return lastbook.getPriority();
    }


    public void updateLastBooks(int bookId,int userId) {
        List<LastBook> lastBooks = getLastBook(userId);
        User user = MyCommon.getUserService().getById(userId);


        Books book = MyCommon.getBookService().getById(bookId);
        if (lastBooks.isEmpty())
        {
            LastBook lastBookNew = new LastBook(user,book,1);
            persist(lastBookNew);
            return;
        }
        int maxPriority = getMaxPriority(userId);
        int minPriority = getMinPriority(userId);
        for (LastBook lastbook: lastBooks) {
            if ((lastbook.getBook().getId() == bookId) && lastbook.getPriority() != maxPriority){
                lastbook.setPriority(maxPriority + 1);
                update(lastbook);
                return;
            } else if (lastbook.getBook().getId() == bookId) {
                return;
            }
        }

        if (lastBooks.size() >= 15){
            delete(getBookWithMinPriority(userId));
            for (LastBook lastbook: lastBooks) {
                lastbook.setPriority(lastbook.getPriority() - minPriority + 1);
                update(lastbook);
            }
            maxPriority = getMaxPriority(userId);

        }
        LastBook lastBookNew = new LastBook(user,book,maxPriority + 1);
        persist(lastBookNew);
    }

    public ListBooksId getLastBooks(int userId){
        List<LastBook> lastBooks = getLastBook(userId);
        ListBooksId listBooksId = new ListBooksId();
        if (lastBooks.isEmpty()){
            return listBooksId;
        }
        for (LastBook lastbook: lastBooks) {
            listBooksId.getBooksId().add(lastbook.getBook().getId());
        }
        return listBooksId;
    }

}
