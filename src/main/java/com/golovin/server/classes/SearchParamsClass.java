package com.golovin.server.classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SearchParamsClass {
    @JsonProperty
    String text;
    @JsonProperty
    int genreId;
    @JsonProperty
    int sortingType;

    public SearchParamsClass() {
    }

    public SearchParamsClass(String text, int genreId, int sortingType) {
        this.text = text;
        this.genreId = genreId;
        this.sortingType = sortingType;
    }
}
