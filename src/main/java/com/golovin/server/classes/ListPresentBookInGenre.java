package com.golovin.server.classes;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;

public class ListPresentBookInGenre {
    @JsonDeserialize(as = ArrayList.class, contentAs = PresentBookInGenre.class)
    ArrayList<PresentBookInGenre> genres;

    public ListPresentBookInGenre() {
        genres = new ArrayList<>();
    }

    public ArrayList<PresentBookInGenre> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<PresentBookInGenre> genres) {
        this.genres = genres;
    }

}
