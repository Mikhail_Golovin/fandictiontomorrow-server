package com.golovin.server.DAO;

import com.golovin.server.DAOInterfaces.CompletenessDaoInterface;
import com.golovin.server.models.Completeness;
import com.golovin.server.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class CompletenessDao implements CompletenessDaoInterface<Completeness, Integer> {


    private Session currentSession;

    private Transaction currentTransaction;

    public CompletenessDao() {
    }

    public Session openCurrentSession() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return currentSession;
    }

    public void closeCurrentSession(Session session) {
        session.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public Completeness findById(Session session, Integer id) {
        Completeness completeness = (Completeness) session.get(Completeness.class, id);
        return completeness;
    }

}
